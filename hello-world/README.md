# Hello World

## Use it

```yaml
stages:
  - 🎉greetings

include:
  - project: 'k33g/easy-ci-cd'
    file: 'hello-world/hello-world.gitlab-ci.yml'

#----------------------------
# say hello world
#----------------------------
👋hello-world:
  stage: 🎉greetings
  extends: .hello-world
```
